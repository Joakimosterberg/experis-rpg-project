package equipments;

/**
 * Class for representing Weapon objects in the application.
 */

public class Weapon extends Item {
    private int damage;
    private double attackSpeed;
    private double weaponDps;
    private WeaponTypes weaponTypes;

    public Weapon() {
    }

    /**
     * Constructor for a new weapon. Involves calculating weapon dps which is damage * attackspeed
     * @param name : String
     * @param levelReq : int
     * @param slot : Slot
     * @param damage : int
     * @param attackSpeed : double
     * @param weaponTypes : WeaponTypes
     */

    public Weapon(String name, int levelReq, Slot slot, int damage, double attackSpeed, WeaponTypes weaponTypes) {
        super(name, levelReq, slot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponTypes = weaponTypes;
        setWeaponDps();
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponTypes getWeaponTypes() {
        return weaponTypes;
    }

    public void setWeaponTypes(WeaponTypes weaponTypes) {
        this.weaponTypes = weaponTypes;
    }

    public double getWeaponDPS() {
        return weaponDps;
    }

    public void setWeaponDps() {
        this.weaponDps = this.damage * this.attackSpeed;
    }

    @Override
    public int getStrength() {
        return 0;
    }

    @Override
    public int getDexterity() {
        return 0;
    }

    @Override
    public int getVitality() {
        return 0;
    }

    @Override
    public int getIntelligence() {
        return 0;
    }
}

package equipments;

/**
 * Represents an enum with various weapon types which exist in the application.
 */

public enum WeaponTypes {
    AXES,
    BOWS,
    DAGGERS,
    HAMMERS,
    STAFFS,
    SWORDS,
    WANDS

}

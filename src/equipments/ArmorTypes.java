package equipments;

/**
 *
 *  Represents an enum with various armor types which exist in the application.
 *  Each armor type has 4 attributes, which enhances each heroes primary attributes.
 */

public enum ArmorTypes {
    CLOTH(1, 3, 10, 2),
    LEATHER(1, 10, 1, 10),
    MAIL(5, 3, 1, 9),
    PLATE(1, 0, 0, 2);

    public int strength;
    public int dexterity;
    public int intelligence;
    public int vitality;

    ArmorTypes(int strength, int dexterity, int intelligence, int vitality) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
}

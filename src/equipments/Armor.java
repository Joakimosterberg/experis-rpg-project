package equipments;

/**
 * Class for representing Armor objects in the application.
 */

import attributes.PrimaryAttributes;

public class Armor extends Item {
    private ArmorTypes armorTypes;
    private PrimaryAttributes primaryAttributes;
    private int strength;
    private int dexterity;
    private int vitality;
    private int intelligence;

    public Armor() {
    }

    /**
     * Constructor for a new Armor object. Setting primary attributes on the object.
     * @param name : String
     * @param levelReq : int
     * @param slot : slot
     * @param armorTypes : armorTypes
     */
    public Armor(String name, int levelReq, Slot slot, ArmorTypes armorTypes) {
        super(name, levelReq, slot);
        this.armorTypes = armorTypes;
        strength = armorTypes.strength;
        dexterity = armorTypes.dexterity;
        vitality = armorTypes.vitality;
        intelligence = armorTypes.intelligence;
    }

    public ArmorTypes getArmorTypes() {
        return armorTypes;
    }

    public void setArmorTypes(ArmorTypes armorTypes) {
        this.armorTypes = armorTypes;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}

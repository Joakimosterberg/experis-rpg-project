package equipments;

/**
 * IEquip interface for equiping weapons and armors in the application. Has boolean for checking valid type and level requirement for a weapon or armor object.
 */

import exceptions.ArmorException;
import exceptions.WeaponException;

public interface IEquip {
    boolean equipWeapon(Slot slot, Weapon weapon) throws WeaponException;

    boolean equipArmor(Slot slot, Armor armor) throws ArmorException;

    Item getItem(Slot slot);
    }

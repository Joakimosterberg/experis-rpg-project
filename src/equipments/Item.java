package equipments;

public abstract class Item {
    protected String name;
    protected int levelReq;
    protected Slot slot;

    public Item() {
    }

    public Item(String name, int levelReq, Slot slot) {
        this.name = name;
        this.levelReq = levelReq;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevelReq() {
        return levelReq;
    }

    public void setLevelReq(int levelReq) {
        this.levelReq = levelReq;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public abstract int getStrength();
    public abstract int getDexterity();
    public abstract int getVitality();
    public abstract int getIntelligence();
}

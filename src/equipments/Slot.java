package equipments;

/**
 * There are four available slots in the project, which a hero can equip an armor or weapon
 */

public enum Slot {
    HEAD,
    CHEST,
    LEGS,
    WEAPON
}

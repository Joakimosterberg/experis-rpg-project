package characters;

import attributes.PrimaryAttributes;
import equipments.*;
import exceptions.ArmorException;
import exceptions.WeaponException;

public class Mage extends Hero {

    public Mage() {
    }

    /**
     * Create Mage object with parent constructor and set basePrimaryAttributes and secondaryAttributes.
     * @param name : String
     * @param level : int
     */
    public Mage(String name, int level) {
        super(name, level);
        setBasePrimaryAttributes(basePrimaryAttributes);
        setSecondaryAttributes();
        equipment.put(Slot.HEAD, new Armor());
        equipment.put(Slot.CHEST, new Armor());
        equipment.put(Slot.LEGS, new Armor());
    }

    @Override
    public PrimaryAttributes getBasePrimaryAttributes() {
        return super.getBasePrimaryAttributes();
    }

    /**
     * set basePrimaryAttributes for a Mage
     * @param basePrimaryAttributes : basePrimaryAttributes
     */
    @Override
    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        super.setBasePrimaryAttributes(basePrimaryAttributes);
        this.basePrimaryAttributes.setStrength(1);
        this.basePrimaryAttributes.setDexterity(1);
        this.basePrimaryAttributes.setIntelligence(8);
        this.basePrimaryAttributes.setVitality(5);

        this.totalPrimaryAttributes.setStrength(1);
        this.totalPrimaryAttributes.setDexterity(1);
        this.totalPrimaryAttributes.setIntelligence(8);
        this.totalPrimaryAttributes.setVitality(5);
    }

    @Override
    public PrimaryAttributes getTotalPrimaryAttributes() {
        return super.getTotalPrimaryAttributes();
    }

    @Override
    public double getCharacterDPS() {
        return this.characterDPS;
    }

    /**
     * setCharacterDPS based on weapon and the heroes primary attribute
     * @param weapon : Weapon
     */
    @Override
    public void setCharacterDPS(Weapon weapon) {
        this.characterDPS = (weapon.getWeaponDPS() * (1.0 + (getTotalPrimaryAttributes().getIntelligence() / 100.0)));
    }

    /**
     * LevelUp method for Mage with specific stats rate for this hero.
     */
    @Override
    public void levelUp() {
        basePrimaryAttributes.setStrength(basePrimaryAttributes.getStrength() + 1);
        basePrimaryAttributes.setVitality(basePrimaryAttributes.getVitality() + 3);
        basePrimaryAttributes.setDexterity(basePrimaryAttributes.getDexterity() + 1);
        basePrimaryAttributes.setIntelligence(basePrimaryAttributes.getIntelligence() + 5);
        setTotalPrimaryAttributes();
        setSecondaryAttributes();
        setLevel(getLevel() + 1);
    }

    /**
     * Exception which checks that Mage only can equip Staff or Wand, else false
     * @param slot : Slot
     * @param weapon : Weapon
     * @return : true
     * @throws WeaponException : Throws exception for wrong weapon type and weapon level
     */
    @Override
    public boolean equipWeapon(Slot slot, Weapon weapon) throws WeaponException {
        boolean validWeapon = false;
        boolean validWeaponLevel = false;

        if (weapon.getWeaponTypes() == WeaponTypes.STAFFS || weapon.getWeaponTypes() == WeaponTypes.WANDS) {
            validWeapon = true;

            if (this.getLevel() >= weapon.getLevelReq()) {
                equipment.put(slot, weapon);
                validWeaponLevel = true;
            }
        }
        if(!validWeapon) {
            throw new WeaponException("Wrong weapon type..");

        }
        else {
            if (!validWeaponLevel) {
                throw new WeaponException("This weapon has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    /**
     * Exception to check that a Mage only can equip Cloth as ArmorType
     * @param slot : Slot
     * @param armor : Armor
     * @return : true
     * @throws ArmorException : Throws Exception for wrong armor type and armor level
     */
    @Override
    public boolean equipArmor(Slot slot, Armor armor) throws ArmorException {
        boolean validArmor = false;
        boolean validArmorLevel = false;

        if (armor.getArmorTypes() == ArmorTypes.CLOTH) {
            validArmor = true;

            if (this.getLevel() >= armor.getLevelReq()) {
                equipment.put(slot, armor);
                validArmorLevel = true;
            }
        }
        if (!validArmor) {
            throw new ArmorException("Wrong armor type..");
        }
        else {
            if (!validArmorLevel){
                throw new ArmorException("Armor has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    @Override
    public Item getItem(Slot slot) {
        return equipment.get(slot);
    }

}

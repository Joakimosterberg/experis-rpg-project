package characters;

import attributes.PrimaryAttributes;
import equipments.*;
import exceptions.ArmorException;
import exceptions.WeaponException;

public class Warrior extends Hero{

    public Warrior() {
    }

    /**
     * Create Warrior object with parent constructor and set basePrimaryAttributes and secondaryAttributes.
     * @param name : String
     * @param level : int
     */
    public Warrior(String name, int level) {
        super(name, level);
        setBasePrimaryAttributes(basePrimaryAttributes);
        setSecondaryAttributes();
        equipment.put(Slot.HEAD, new Armor());
        equipment.put(Slot.CHEST, new Armor());
        equipment.put(Slot.LEGS, new Armor());
    }

    @Override
    public PrimaryAttributes getBasePrimaryAttributes() {
        return super.getBasePrimaryAttributes();
    }

    /**
     * set basePrimaryAttributes for a Warrior
     * @param basePrimaryAttributes : PrimaryAttributes
     */
    @Override
    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        super.setBasePrimaryAttributes(basePrimaryAttributes);
        this.basePrimaryAttributes.setVitality(10);
        this.basePrimaryAttributes.setStrength(5);
        this.basePrimaryAttributes.setDexterity(2);
        this.basePrimaryAttributes.setIntelligence(1);

        this.totalPrimaryAttributes.setVitality(10);
        this.totalPrimaryAttributes.setStrength(5);
        this.totalPrimaryAttributes.setDexterity(2);
        this.totalPrimaryAttributes.setIntelligence(1);
    }

    @Override
    public PrimaryAttributes getTotalPrimaryAttributes() {
        return super.getTotalPrimaryAttributes();
    }

    @Override
    public double getCharacterDPS() {
        return characterDPS;
    }

    /**
     * setCharacterDPS based on weapon and the heroes primary attribute
     * @param weapon : Weapon
     */
    @Override
    public void setCharacterDPS(Weapon weapon) {
        characterDPS = (weapon.getWeaponDPS() * (1.0 + (getTotalPrimaryAttributes().getStrength() / 100.0)));
    }
    /**
     * LevelUp method for Warrior with specific stats rate for this hero.
     */
    @Override
    public void levelUp() {
        basePrimaryAttributes.setStrength(basePrimaryAttributes.getStrength() + 3);
        basePrimaryAttributes.setIntelligence(basePrimaryAttributes.getIntelligence() + 1);
        basePrimaryAttributes.setVitality(basePrimaryAttributes.getVitality() + 5);
        basePrimaryAttributes.setDexterity(basePrimaryAttributes.getDexterity() + 2);
        setTotalPrimaryAttributes();
        setSecondaryAttributes();
        setLevel(getLevel() + 1);
    }

    /**
     * Exception which checks that Warrior only can equip Axe, Hammer or Sword, else false
     * @param slot : Slot
     * @param weapon : Weapon
     * @return : true
     * @throws WeaponException : Throws exception for wrong weapon type and weapon level
     */
    @Override
    public boolean equipWeapon(Slot slot, Weapon weapon) throws WeaponException {
        boolean validWeapon = false;
        boolean validWeaponLevel = false;

        if (weapon.getWeaponTypes() == WeaponTypes.AXES || weapon.getWeaponTypes() == WeaponTypes.HAMMERS || weapon.getWeaponTypes() == WeaponTypes.SWORDS) {
            validWeapon = true;

            if (this.getLevel() >= weapon.getLevelReq()) {
                equipment.put(slot, weapon);
                validWeaponLevel = true;
            }
        }
        if(!validWeapon) {
            throw new WeaponException("Wrong weapon type..");

        }
        else {
            if (!validWeaponLevel) {
                throw new WeaponException("This weapon has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    /**
     * Exception to check that a Warrior only can equip Plate and Mail as ArmorType
     * @param slot : Slot
     * @param armor : Armor
     * @return : true
     * @throws ArmorException : Throws Exception for wrong armor type and armor level
     */
    @Override
    public boolean equipArmor(Slot slot, Armor armor) throws ArmorException {
        boolean validArmor = false;
        boolean validArmorLevel = false;

        if (armor.getArmorTypes() == ArmorTypes.PLATE || armor.getArmorTypes() == ArmorTypes.MAIL) {
            validArmor = true;

            if (this.getLevel() >= armor.getLevelReq()) {
                equipment.put(slot, armor);
                validArmorLevel = true;
            }
        }
        if (!validArmor) {
            throw new ArmorException("Wrong armor type..");
        }
        else {
            if (!validArmorLevel){
                throw new ArmorException("Armor has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    @Override
    public Item getItem(Slot slot) {
        return equipment.get(slot);
    }
}

package characters;

import equipments.*;
import exceptions.ArmorException;
import exceptions.WeaponException;

public class Main {

    public static void main(String[] args) throws ArmorException, WeaponException {

        Warrior aWarriorCharacter = new Warrior("The wall", 1);
        Weapon aWeapon = new Weapon("Classic Axe", 1, Slot.WEAPON, 20, 1.8, WeaponTypes.HAMMERS);
        aWarriorCharacter.equipWeapon(Slot.WEAPON, aWeapon);
        aWarriorCharacter.setCharacterDPS(aWeapon);

        Armor plateHelmet = new Armor("Plate head", 1, Slot.HEAD, ArmorTypes.PLATE);
        Armor plateChest = new Armor("Plate chest", 1, Slot.CHEST, ArmorTypes.PLATE);
        Armor plateLegs = new Armor("Plate legs", 1, Slot.LEGS, ArmorTypes.PLATE);
        aWarriorCharacter.equipArmor(Slot.HEAD, plateHelmet);
        aWarriorCharacter.equipArmor(Slot.CHEST, plateChest);
        aWarriorCharacter.equipArmor(Slot.LEGS, plateLegs);
        aWarriorCharacter.setTotalPrimaryAttributes();

        System.out.println(aWarriorCharacter.displayHeroStats());

    }
}
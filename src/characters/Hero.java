package characters;

import attributes.PrimaryAttributes;
import attributes.SecondaryAttributes;
import equipments.*;

import java.util.HashMap;

public abstract class Hero implements ILevel, IEquip {
    private String name;
    private int level;
    protected double characterDPS = 1;
    protected PrimaryAttributes totalPrimaryAttributes = new PrimaryAttributes();
    protected PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes();
    protected SecondaryAttributes secondaryAttributes = new SecondaryAttributes();
    protected HashMap<Slot, Item> equipment = new HashMap<>();

    public Hero() {
    }

    public Hero(String name, int level) {
        this.name = name;
        this.level = level;
    }

    /**
     * Get a heroes starting attributes such as Strength, Vitality, Dexterity, Intelligence
     * @return basePrimaryAttributes
     */
    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.totalPrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength());
        this.totalPrimaryAttributes.setVitality(this.basePrimaryAttributes.getVitality());
        this.totalPrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence());
        this.totalPrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity());
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    /**
     *
     * Total attributes = basePrimaryAttributes + attributes from items such as Head, Chest and Legs.
     */

    public void setTotalPrimaryAttributes() {
        this.totalPrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength() + equipment.get(Slot.HEAD).getStrength() + equipment.get(Slot.CHEST).getStrength() + equipment.get(Slot.LEGS).getStrength());
        this.totalPrimaryAttributes.setVitality(this.basePrimaryAttributes.getVitality() + equipment.get(Slot.HEAD).getVitality() + equipment.get(Slot.CHEST).getVitality() + equipment.get(Slot.LEGS).getVitality());
        this.totalPrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence() + equipment.get(Slot.HEAD).getIntelligence() + equipment.get(Slot.CHEST).getIntelligence() + equipment.get(Slot.LEGS).getIntelligence());
        this.totalPrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity() + equipment.get(Slot.HEAD).getDexterity() + equipment.get(Slot.CHEST).getDexterity() + equipment.get(Slot.LEGS).getDexterity());
    }

    public SecondaryAttributes getSecondaryAttributes() {
        return secondaryAttributes;
    }

    /**
     * Based on totalPrimaryAttributes, calculate secondaryAttributes which is, Health, Armor rating and Elemental resistence
     */
    public void setSecondaryAttributes() {
        this.secondaryAttributes.setHealth(this.totalPrimaryAttributes.getVitality() * 10);
        this.secondaryAttributes.setArmorRating(this.totalPrimaryAttributes.getDexterity() + this.totalPrimaryAttributes.getStrength());
        this.secondaryAttributes.setElementalResistence(this.totalPrimaryAttributes.getIntelligence());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Check so a heroes level cant be 0
     * @param level : int
     */
    public void setLevel(int level) {
        if (getLevel() >= level) {
            throw new IllegalArgumentException();
        } else if (level <= 0) {
            throw new IllegalArgumentException();
        }
        this.level = level;
    }

    /**
     * Displays data from of the object
     * @return : StringBuilder
     */
    public String displayHeroStats() {
        StringBuilder stringbuilder = new StringBuilder();

        stringbuilder.append("Hero name: ").append(getName() + "\n");
        stringbuilder.append("Level: ").append(getLevel() + "\n");
        stringbuilder.append("Strength: ").append(getTotalPrimaryAttributes().getStrength() + "\n");
        stringbuilder.append("Dexterity: ").append(getTotalPrimaryAttributes().getDexterity() + "\n");
        stringbuilder.append("Intelligence: ").append(getTotalPrimaryAttributes().getIntelligence() + "\n");
        stringbuilder.append("Vitality: ").append(getTotalPrimaryAttributes().getVitality() + "\n");
        stringbuilder.append("Health: ").append(getSecondaryAttributes().getHealth() + "\n");
        stringbuilder.append("Elemental Resistance: ").append(getSecondaryAttributes().getElementalResistence() + "\n");
        stringbuilder.append("DPS: ").append(getCharacterDPS());

        return stringbuilder.toString();
    }

    public abstract double getCharacterDPS();

    public abstract void setCharacterDPS(Weapon weapon);
}

package characters;

import attributes.PrimaryAttributes;
import equipments.*;
import exceptions.ArmorException;
import exceptions.WeaponException;

public class Rogue extends Hero{

    public Rogue() {
    }

    /**
     * Create Rogue object with parent constructor and set basePrimaryAttributes and secondaryAttributes.
     * @param name : String
     * @param level : int
     */
    public Rogue(String name, int level) {
        super(name, level);
        setBasePrimaryAttributes(basePrimaryAttributes);
        setSecondaryAttributes();
        equipment.put(Slot.HEAD, new Armor());
        equipment.put(Slot.CHEST, new Armor());
        equipment.put(Slot.LEGS, new Armor());
    }

    @Override
    public PrimaryAttributes getBasePrimaryAttributes() {
        return super.getBasePrimaryAttributes();
    }

    /**
     * set basePrimaryAttributes for a Rogue
     * @param basePrimaryAttributes : Primaryattributes
     */
    @Override
    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        super.setBasePrimaryAttributes(basePrimaryAttributes);
        this.basePrimaryAttributes.setStrength(2);
        this.basePrimaryAttributes.setDexterity(6);
        this.basePrimaryAttributes.setIntelligence(1);
        this.basePrimaryAttributes.setVitality(8);

        this.totalPrimaryAttributes.setStrength(2);
        this.totalPrimaryAttributes.setDexterity(6);
        this.totalPrimaryAttributes.setIntelligence(1);
        this.totalPrimaryAttributes.setVitality(8);
    }

    @Override
    public double getCharacterDPS() {
        return this.characterDPS;
    }

    /**
     * setCharacterDPS based on weapon and the heroes primary attribute
     * @param weapon : Weapon
     */
    @Override
    public void setCharacterDPS(Weapon weapon) {
        this.characterDPS = (weapon.getWeaponDPS() * (1.0 + (getTotalPrimaryAttributes().getDexterity() / 100.0)));
    }

    /**
     * LevelUp method for Rogue with specific stats rate for this hero.
     */
    @Override
    public void levelUp() {
        basePrimaryAttributes.setStrength(basePrimaryAttributes.getStrength() + 1);
        basePrimaryAttributes.setVitality(basePrimaryAttributes.getVitality() + 3);
        basePrimaryAttributes.setDexterity(basePrimaryAttributes.getDexterity() + 4);
        basePrimaryAttributes.setIntelligence(basePrimaryAttributes.getIntelligence() + 1);
        setTotalPrimaryAttributes();
        setSecondaryAttributes();
        setLevel(getLevel() + 1);
    }

    /**
     * Exception which checks that Rogue only can equip Dagger or Sword, else false
     * @param slot : Slot
     * @param weapon : Weapon
     * @return : true
     * @throws WeaponException : Throws exception for wrong weapon type and weapon level
     */
    @Override
    public boolean equipWeapon(Slot slot, Weapon weapon) throws WeaponException {
        boolean validWeapon = false;
        boolean validWeaponLevel = false;

        if (weapon.getWeaponTypes() == WeaponTypes.DAGGERS || weapon.getWeaponTypes() == WeaponTypes.SWORDS) {
            validWeapon = true;

            if (this.getLevel() >= weapon.getLevelReq()) {
                equipment.put(slot, weapon);
                validWeaponLevel = true;
            }
        }
        if(!validWeapon) {
            throw new WeaponException("Wrong weapon type..");

        }
        else {
            if (!validWeaponLevel) {
                throw new WeaponException("This weapon has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    /**
     * Exception to check that a Rogue only can equip Leather and Mail as ArmorType
     * @param slot : Slot
     * @param armor : Armor
     * @return : true
     * @throws ArmorException : Throws Exception for wrong armor type and armor level
     */
    @Override
    public boolean equipArmor(Slot slot, Armor armor) throws ArmorException {
        boolean validArmor = false;
        boolean validArmorLevel = false;

        if (armor.getArmorTypes() == ArmorTypes.LEATHER || armor.getArmorTypes() == ArmorTypes.MAIL) {
            validArmor = true;

            if (this.getLevel() >= armor.getLevelReq()) {
                equipment.put(slot, armor);
                validArmorLevel = true;
            }
        }
        if (!validArmor) {
            throw new ArmorException("Wrong armor type..");
        }
        else {
            if (!validArmorLevel){
                throw new ArmorException("Armor has to high item level for your hero");
            }
        }
        setTotalPrimaryAttributes();
        return true;
    }

    @Override
    public Item getItem(Slot slot) {
        return equipment.get(slot);
    }
}

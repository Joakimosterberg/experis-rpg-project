package characters;

/**
 * Interface for levelUp. The purpose for this is that every subclass has different implementation in their stats, which changes from class to class.
 */

public interface ILevel {
    void levelUp();
}

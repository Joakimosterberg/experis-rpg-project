package exceptions;

/**
 * Exception for valid WeaponType and level requirement
 */

public class WeaponException extends Exception{
    public WeaponException() {
    }

    public WeaponException(String message) {
        super(message);
    }
}

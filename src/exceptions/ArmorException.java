package exceptions;

/**
 * Exception for valid ArmorType and level requirement
 */

public class ArmorException extends Exception{
    public ArmorException() {
    }

    public ArmorException(String message) {
        super(message);
    }
}

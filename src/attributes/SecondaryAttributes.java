package attributes;

/**
 * Secondary Attributes purpose is for defense, which each Hero subclass has.
 */

public class SecondaryAttributes {
    private int Health;
    private int armorRating;
    private int elementalResistence;

    public SecondaryAttributes() {
    }

    public SecondaryAttributes(int health, int armorRating, int elementalResistence) {
        Health = health;
        this.armorRating = armorRating;
        this.elementalResistence = elementalResistence;
    }

    public int getHealth() {
        return Health;
    }

    public void setHealth(int health) {
        Health = health;
    }

    public int getArmorRating() {
        return armorRating;
    }

    public void setArmorRating(int armorRating) {
        this.armorRating = armorRating;
    }

    public int getElementalResistence() {
        return elementalResistence;
    }

    public void setElementalResistence(int elementalResistence) {
        this.elementalResistence = elementalResistence;
    }
}

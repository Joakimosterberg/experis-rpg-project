package testClasses;

import characters.Warrior;
import equipments.*;
import exceptions.ArmorException;
import exceptions.WeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    Warrior testWarrior = new Warrior("Test warrior", 1);   //Warrior test object level 1

    @Test
    void equipHighLevel_Weapon_false() {
        Weapon testWeapon = new Weapon("A common weapon", 2, Slot.WEAPON, 10, 2, WeaponTypes.HAMMERS);
        assertThrows(WeaponException.class,() ->{
            testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        });
    }
    @Test
    void equipHighLevel_Armor_false() {
        Armor testArmor = new Armor("A common armor", 2, Slot.CHEST, ArmorTypes.PLATE);
        assertThrows(ArmorException.class,() -> {
            testWarrior.equipArmor(Slot.CHEST, testArmor);
        });
    }
    @Test
    void equipWrongWeaponType_false() {      //Warrior cant equip STAFFS
        Weapon testWeapon = new Weapon("A common weapon", 1, Slot.WEAPON, 10, 2, WeaponTypes.STAFFS);
        assertThrows(WeaponException.class,() -> {
            testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        });
    }
    @Test
    void equipWrongArmorType_false() {      //Warrior cant equit Leather as armor type
        Armor testArmor = new Armor("A common armor", 1, Slot.CHEST, ArmorTypes.LEATHER);
        assertThrows(ArmorException.class,() -> {
            testWarrior.equipArmor(Slot.CHEST, testArmor);
        });
    }
    @Test
    void equipValidWeapon_true() throws WeaponException {
        Weapon testWeapon = new Weapon("A common weapon", 1, Slot.WEAPON, 10, 2, WeaponTypes.HAMMERS);
        assertTrue(testWarrior.equipWeapon(Slot.WEAPON, testWeapon));
    }
    @Test
    void equipValidArmor_true() throws ArmorException {
        Armor testArmor = new Armor("A common armor", 1, Slot.CHEST, ArmorTypes.PLATE);
        assertTrue(testWarrior.equipArmor(Slot.CHEST, testArmor));
    }
    @Test
    void checkDPS_WithNoWeapon_Equipted_true() {
        assertEquals(testWarrior.getCharacterDPS(), 1 * (1 +(5 / 100)));
    }
    @Test
    void checkDPS_WithValidWeapon_true() throws WeaponException {
        Weapon testWeapon = new Weapon("A common weapon", 1, Slot.WEAPON, 7, 1.1, WeaponTypes.AXES);
        testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        testWarrior.setCharacterDPS(testWeapon);
        assertEquals(testWarrior.getCharacterDPS(), (7 * 1.1) * (1+(5/100)));
    }
    @Test
    void checkDps_WithValidWeaponAndArmor_true() throws WeaponException, ArmorException {
        Weapon testWeapon = new Weapon("A common weapon", 1, Slot.WEAPON, 7, 1.1, WeaponTypes.AXES);
        Armor testArmor = new Armor("A common armor", 1, Slot.CHEST, ArmorTypes.PLATE);
        testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        testWarrior.equipArmor(Slot.CHEST, testArmor);
        testWarrior.setCharacterDPS(testWeapon);
        assertEquals(testWarrior.getCharacterDPS(), ((7.0*1.1)*(1.0+((5.0+1.0)/100.0))));
    }
}
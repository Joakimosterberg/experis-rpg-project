package testClasses;

import characters.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    void checkLevel_true() {
        Warrior testWarrior = new Warrior("The wall", 1);
        assertEquals(testWarrior.getLevel(), 1);
    }
    @Test
    void checkLevelUp_true() {
        Warrior testWarrior = new Warrior("The wall", 1);
        testWarrior.levelUp();
        assertEquals(testWarrior.getLevel(), 2);
    }

    @Test
    void checkLevel_up_false() {
        assertThrows(IllegalArgumentException.class,() -> {
            Warrior testWarrior = new Warrior("The wall", 1);
            testWarrior.setLevel(0);
        });
    }

    //4 following test methods for each hero. Tests involves checking start attributes.
    @Test
    void checkStartStats_Mage_true() {
        Mage testMage = new Mage("Test mage", 1);
        assertEquals(testMage.getBasePrimaryAttributes().getDexterity(), 1);
        assertEquals(testMage.getBasePrimaryAttributes().getStrength(), 1);
        assertEquals(testMage.getBasePrimaryAttributes().getVitality(), 5);
        assertEquals(testMage.getBasePrimaryAttributes().getIntelligence(), 8);
    }
    @Test
    void checkStartStats_Rogue_true() {
        Rogue testRogue = new Rogue("Test rogue", 1);
        assertEquals(testRogue.getBasePrimaryAttributes().getDexterity(), 6);
        assertEquals(testRogue.getBasePrimaryAttributes().getStrength(), 2);
        assertEquals(testRogue.getBasePrimaryAttributes().getVitality(), 8);
        assertEquals(testRogue.getBasePrimaryAttributes().getIntelligence(), 1);
    }
    @Test
    void checkStartStats_Ranger_true() {
        Ranger testRanger = new Ranger("Test ranger", 1);
        assertEquals(testRanger.getBasePrimaryAttributes().getDexterity(), 7);
        assertEquals(testRanger.getBasePrimaryAttributes().getStrength(), 1);
        assertEquals(testRanger.getBasePrimaryAttributes().getVitality(), 8);
        assertEquals(testRanger.getBasePrimaryAttributes().getIntelligence(), 1);
    }
    @Test
    void checkStartStats_Warrior_true() {
        Warrior testWarrior = new Warrior("Test warrior", 1);
        assertEquals(testWarrior.getBasePrimaryAttributes().getDexterity(), 2);
        assertEquals(testWarrior.getBasePrimaryAttributes().getStrength(), 5);
        assertEquals(testWarrior.getBasePrimaryAttributes().getVitality(), 10);
        assertEquals(testWarrior.getBasePrimaryAttributes().getIntelligence(), 1);
    }

    //4 following test methods for each hero. Tests involves checking each heroes attributes when leveling.
    @Test
    void checkAttributes_levelUp_warrior_true() {
        Warrior testWarrior = new Warrior("Test warrior", 1);
        testWarrior.levelUp();
        assertEquals(testWarrior.getTotalPrimaryAttributes().getStrength(),8);
        assertEquals(testWarrior.getTotalPrimaryAttributes().getVitality(),15);
        assertEquals(testWarrior.getTotalPrimaryAttributes().getDexterity(),4);
        assertEquals(testWarrior.getTotalPrimaryAttributes().getIntelligence(),2);
    }
    @Test
    void checkAttributes_levelUp_Ranger_true() {
        Ranger testRanger = new Ranger("Test ranger", 1);
        testRanger.levelUp();
        assertEquals(testRanger.getTotalPrimaryAttributes().getStrength(),2);
        assertEquals(testRanger.getTotalPrimaryAttributes().getVitality(),10);
        assertEquals(testRanger.getTotalPrimaryAttributes().getDexterity(),12);
        assertEquals(testRanger.getTotalPrimaryAttributes().getIntelligence(),2);
    }
    @Test
    void checkAttributes_levelUp_Mage_true() {
        Mage testMage = new Mage("Test mage", 1);
        testMage.levelUp();
        assertEquals(testMage.getTotalPrimaryAttributes().getStrength(),2);
        assertEquals(testMage.getTotalPrimaryAttributes().getVitality(),8);
        assertEquals(testMage.getTotalPrimaryAttributes().getDexterity(),2);
        assertEquals(testMage.getTotalPrimaryAttributes().getIntelligence(),13);
    }
    @Test
    void checkAttributes_levelUp_Rogue_true() {
        Rogue testRogue = new Rogue("Test rogue", 1);
        testRogue.levelUp();
        assertEquals(testRogue.getTotalPrimaryAttributes().getStrength(),3);
        assertEquals(testRogue.getTotalPrimaryAttributes().getVitality(),11);
        assertEquals(testRogue.getTotalPrimaryAttributes().getDexterity(),10);
        assertEquals(testRogue.getTotalPrimaryAttributes().getIntelligence(),2);
    }
    @Test
    void checkSecondaryStats_true() {
        Warrior testWarrior = new Warrior("Test warrior", 1);
        testWarrior.levelUp();
        assertEquals(testWarrior.getSecondaryAttributes().getHealth(), 150);
        assertEquals(testWarrior.getSecondaryAttributes().getArmorRating(), 12);
        assertEquals(testWarrior.getSecondaryAttributes().getElementalResistence(), 2);
    }
}
Java assignment 1 in the Experis Academy Java program 2021. This project involves a console application in Java which involves various kinds of RPG heroes such as a Warrior, Ranger, Rogue and Mage. This heroes can equip armors and weapons for each hero which adding stats. The stats is used to calculate a heroes damage and also secondary stats such as health, armor and elemental resistance. 
In the project there are two abstract classes, Hero and Item, which contain this data. The aforementioned subclasses inherit from abstract class Hero. Two classes inherits item, Armor and Weapon, which works together with two Enums to work with various armor and weapon types.   

This project started with a basic UML-diagram to illustrate classes and their associations. The diagram is also placed in the repository. 

Lastly, testing has been done with JUnit to check methods for creating i.e, various heroes and equiping certain equipment such as weapons and armors.
